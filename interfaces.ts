import { Professor, Student } from "./university";

export interface Person {
    id?: string;
    name: string;
    age: number;
}

export interface Course {
    courseId: string;
    name: string;
    professor: Professor;
    numberOfClasses?: number;
    hasFinalTest?: boolean;
    faculty?: string;
}

export interface Grade {
    course: Course;
    student: Student;
    grade: number;
}