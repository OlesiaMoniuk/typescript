export enum Capacity {
    Rector = 'RECTOR',
    Dean = 'DEAN',
    Secretary = 'SECRETARY',
    Professor = 'PROFESSOR',
    Student = 'STUDENT', 
}