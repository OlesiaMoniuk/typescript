import { Capacity } from "./capacity";
import { Person, Course, Grade } from "./interfaces";


type Employee = Person & { capacity: Capacity; salary: number };

abstract class EmployeeBase {
    private _salary: number;

    constructor(public name: string, public age: number, public capacity: Capacity) {
        this._salary = 0;
    }

    get salary(): number {
        return this._salary;
    }

    set salary(value: number) {
        this._salary = value;
    }

    abstract calculateSalary(): number;
}

class Rector extends EmployeeBase {
    calculateSalary(): number {
        return 50000;
    }
    manageTheUni(massage: Readonly<string>) {
        massage = "I'm the boss!";
        console.log(`Rector says " ${massage}`);
    }
}

class Dean extends EmployeeBase {
    calculateSalary(): number {
        return 35000;
    }

}

@validateProfessorClass
export class Professor extends EmployeeBase {
    @validateProperty
    public age: number;
    private _faculty: string;

    calculateSalary(): number {
        return 10000;
    }
    constructor(name: string, age: number, faculty: string) {
        super(name, age, Capacity.Professor);
        this.age = age;
        this._faculty = faculty;
    }

    get faculty(): string {
        return this._faculty;
    }

    set faculty(value: string) {
        this._faculty = value;
    }
    greetStudents(@validateGreeting('Hello, newcomers!') greeting: string) {
        console.log(`${this.name} says : ${greeting}`);
    }
}

export class Student implements Person {
    constructor(public name: string, public age: number, public id: string) {
    }

    testPassed(grade: number): boolean {
        return grade > 70;
    }
    getResult(grade: number): string {
        return this.testPassed(grade) ? "Test passed! :)" : 'Test failed! :(';
    }
}

type CourseInfo = "english" | "IT" | "ukrainian";

class Markbook {
    private grades: Grade[] = [];

    addGrade(course: Course, student: Student, grade: number): void {
        const newGrade: Grade = { course, student, grade };
        this.grades.push(newGrade);
    }
}

class University<T extends Person> {
    private _employees: Employee[] = [];
    private _students: T[] = [];
    private markbook: Markbook;
    private courses: Course[] = [];

    constructor(public name: string) {
        this.markbook = new Markbook;
    }
    @LogGetter
    get employees(): Employee[] {
        return this._employees;
    }
    @LogSetter
    get students(): T[] {
        return this._students;
    }

    get allCourses(): Course[] {
        return this.courses;
    }

    get grades(): Markbook {
        return this.markbook;
    }

    @observeTheProcessOfAddingStaff
    addEmployee(employee: Employee): void {
        this._employees.push(employee);
    }
    @observeTheProcessOfAddingStaff
    addStudent(student: T): void {
        this._students.push(student);
    }

    removeEmployee(name: string): void {
        this._employees = this._employees.filter((employee) => employee.name !== name);
    }

    removeStudent(studentId: string): void {
        this._students = this._students.filter((student) => student.id !== studentId);
    }

    addGrade(course: Partial<Course>, student: Partial<Student>, grade: number): void {
        this.markbook.addGrade(course as Course, student as Student, grade);
    }

    addCourse(course: Course): void {
        this.courses.push(course);
        console.log(`Add course: ${JSON.stringify(course)}`);
    }
    removeCourse(id: string): void {
        this.courses = this.courses.filter((course) => course.courseId !== id);
        console.log(`Remove course ID ${id}.}`);
    }
}
interface SpecialCourse extends Course {
    special: boolean;
}
interface SpecialStudent extends Person {
    special: boolean;
}

// Декоратор для метода
function observeTheProcessOfAddingStaff(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;
    descriptor.value = function (...args: any[]) {
        console.log(`Adding a personnel : ${args[0].name}`);
        const result = originalMethod.apply(this, args);
        console.log(` Personnel was added!`);
        return result;
    }
}

// Декоратор для класу
function validateProfessorClass<T extends { new(...args: any[]): {} }>(constructor: T) {
    return class extends constructor {
        constructor(...args: any[]) {
            super(...args);
            const name = args[0];
            if (typeof name !== 'string' || name.trim() === '') {
                throw new Error('Invalid name provided');
            }
        }
    };
}
// дукоратор для проперті
function validateProperty(target: any, key: string) {
    let value = target[key];
    const getter = function () {
        return value;
    };
    const setter = function (newValue: any) {
        if (typeof newValue !== 'number' || newValue <= 0) {
            throw new Error(`Invalid value assigned to property ${key}`);
        }

        value = newValue;
    };

    Object.defineProperty(target, key, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true,
    });
}

// тут валідує аргумент мeтоду
function validateGreeting(argName: string) {
    return function (target: any, methodName: string, parameterIndex: number) {
        const originalMethod = target[methodName];
        target[methodName] = function (...args: any[]) {
            const argValue = args[parameterIndex];
            if (typeof argValue !== 'string' || argValue.length === 0) {
                throw new Error(`Invalid argument value for ${argName}`);
            }
            return originalMethod.apply(this, args);
        };
    };
}

function LogGetter(target: Object, propertyKey: string | symbol, descriptor: PropertyDescriptor): void {
    const originalGetter = descriptor.get;

    descriptor.get = function (this: any): any {
        console.log(`Accessing getter: ${String(propertyKey)}`);
        return originalGetter?.apply(this);
    };
}

function LogSetter(target: Object, propertyKey: string | symbol, descriptor: PropertyDescriptor): void {
    const originalSetter = descriptor.set;

    descriptor.set = function (this: any, value: any): void {
        console.log(`Setting setter: ${String(propertyKey)} to ${JSON.stringify(value)}`);
        originalSetter?.apply(this, [value]);
    };
}

const university = new University<Person>('Prinston');

const employee1 = new Professor('Bogdan', 45, 'Linguistics');
const employee2 = new Professor('Victoria', 28, 'IT');
university.addEmployee(employee1);

const course: Course = { courseId: '1', name: 'English', professor: employee1 };
const specialCourse: SpecialCourse = {
    courseId: '3',
    name: 'Informational Technologies',
    special: true,
    professor: employee2,
} as SpecialCourse;
const subjectInfo: Record<CourseInfo, Course> = {
    english: { courseId: '1', name: 'English', professor: employee1, numberOfClasses: 10, hasFinalTest: true },
    IT: { courseId: '2', name: 'IT', professor: employee2, numberOfClasses: 5, hasFinalTest: false },
    ukrainian: { courseId: '3', name: 'Ukrainian', professor: employee1, numberOfClasses: 20, hasFinalTest: true }
}
university.addCourse(specialCourse);
university.addCourse(course);


const student1 = new Student('Ted', 20, '1');
const student2 = new Student('Bob', 22, '2');
const student3 = new Student('Maria', 18, '3');
const student4 = new Student('Liza', 25, '4');
const specialStudent: SpecialStudent = {
    name: 'Andrew',
    age: 45,
    special: true,
    id: '5',
} as SpecialStudent;

university.addStudent(student1);
university.addStudent(student2);
university.addStudent(student3);
university.addStudent(student4);
university.addStudent(specialStudent);

university.addGrade(course, student1, 99);
university.addGrade(course, student2, 34);
university.addGrade(course, student3, 78);
university.addGrade(course, student4, 100);

console.log('University employees:', university.employees);
console.log('University students:', university.students);
console.log('University courses:', university.allCourses);
console.log("Students' grades:", JSON.stringify(university.grades));
// type guard
function finalExamResult(student: Student, grade: number): void {
    if (student.testPassed(grade)) {
        console.log(`${student.name} stays for the next year!`);
    } else {
        console.log(`${student.name} has to leave the Uni!`);
    }
}
finalExamResult(student1, 99);
finalExamResult(student2, 34);
finalExamResult(student3, 78);
finalExamResult(student4, 100);
console.log('Information about subjects:', JSON.stringify(subjectInfo));

